use std::{fs::File, io::Write};
use reqwest::{IntoUrl};
use sqlx::postgres::PgPoolOptions;
use sqlx::migrate::Migrator;
use sqlx::FromRow;
use sqlx::PgPool;
use dotenv::dotenv;
use std::env;
use tokio;

// static MIGRATOR: Migrator = sqlx::migrate!("./migrations");

#[derive(Debug, Default, FromRow)]
pub struct Item {
    name: String,
    id: String,
    rlvl: Option<i32>,
    rstr: Option<i32>,
    rdex: Option<i32>,
    rint: Option<i32>,
    rluk: Option<i32>,
    category: Option<String>,
    attack_speed: Option<i32>,
    weapon_attack: Option<i32>,
    magic_attack: Option<i32>,
    strn: Option<i32>,
    dex: Option<i32>,
    inte: Option<i32>,
    luk: Option<i32>,
    hp: Option<i32>,
    mp: Option<i32>,
    weapon_def: Option<i32>,
    magic_def: Option<i32>,
    slots_remaining: Option<i32>,
    avoidability: Option<i32>,
    accuracy: Option<i32>,
    speed: Option<i32>,
    jump: Option<i32>,
}

const BASE_URL: &str = "https://maplelegends.com";

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    // Setup environment vars
    env::set_var("RUST_BACKTRACE", "1");
    dotenv().ok();

    // Setup database
    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    println!("Database URL: {}", database_url);

    let pool = PgPoolOptions::new()
        .max_connections(5)
        .connect(&database_url)
        .await?;

    // MIGRATOR.run(&pool).await?;

    let url = format!("{}{}", BASE_URL, "/lib");
    let html_content = reqwest::get(url.clone()).await?.text().await?;
    let document = scraper::Html::parse_document(&html_content);

    // Hash set of all item page links to be scraped
    let mut item_pages: std::collections::HashSet<String> = std::collections::HashSet::new();

    // Get the links of each base page to be crawled by looping over the dropdown menus and
    // ignoring links that take us back to the index.
    //
    // For each link to a base page call `crawl_item_type` to get all the item page links for later
    // scraping
    let dropdown_selector = scraper::Selector::parse(r#"li[class="dropdown "]"#).unwrap();
    let base_page_selector = scraper::Selector::parse("a").unwrap();

    for dropdown in document.select(&dropdown_selector) {
        for base_page in dropdown.select(&base_page_selector) {
            println!("Base page found in a dropdown with link: {:?}", base_page.value().attr("href"));
            match base_page.value().attr("href") {
                Some("#") => (),
                Some(link) => {
                    println!("Crawling through {}{} for item links", BASE_URL, link);
                    let _ = crawl_item_type(format!("{}{}", BASE_URL, link), &mut item_pages).await;
                },
                None => ()
            }
        }
    }

    // For each item page link collect call the `scrape_item_page` function and add the resulting
    // item struct to the db
    let mut count = 0;
    for item_rel_link in &item_pages {
        count += 1;
        let item_url = format!("{}{}", BASE_URL, item_rel_link);
        let item = scrape_item_page(&item_url).await;
        match item {
            Ok(item)=> {
                match insert_item(&pool, &item).await {
                    Ok(()) => print!("[{}] {}: Successfully added to database", item.id, item.name),
                    Err(e) => print!("[{}] {}: Database insert failure ({})", item.id, item.name, e),
                }
                println!("\t{}/{}", count, item_pages.len());

            },

            Err(e) => {
                println!("[{}]: {}", item_url, e);
                continue;
            },
        }
    }
    Ok(())
}

    
async fn crawl_item_type(base_url: String, item_pages: &mut std::collections::HashSet<String>) -> Result<(), Box<dyn std::error::Error>>
{
    // Url to be modified by each relative link found while crawling
    let mut url = base_url.clone();

    // Counter of table rows used to determine the number of items on the current page
    let mut tr_count = 1;

    // Check the current page for items, if it has some add their links to the hashset for later
    // scraping. Find the link to the next page and repeat until there are no items on the page
    while tr_count > 0 {
        let html_content = reqwest::get(url.clone()).await?.text().await?;
        let document = scraper::Html::parse_document(&html_content);

        // Find the table rows in only the main table to determine how many items are on the page.
        // This is used for determining when to stop crawling (when 0 items on page stop)
        // The count is - 1 because the row of headers is always counted
        let mt_selector = scraper::Selector::parse(r#"div[id="mainTable"]"#).unwrap();
        let tr_selector = scraper::Selector::parse("tr").unwrap();
        let main_table = document.select(&mt_selector).next().unwrap();

        tr_count = main_table.select(&tr_selector).count() - 1;
        match tr_count {
            0 => {
                println!("No items on page");
                break;
            },
            n => {
                println!("{n} items on page, collecting links...");

                let item_link_selector = scraper::Selector::parse("td > a").unwrap();
                for item_link in document.select(&item_link_selector) {
                    match item_link.value().attr("href").map(str::to_owned) {
                        Some(link) => {
                            if !item_pages.contains(&link) {
                                item_pages.insert(link);
                            }
                        },
                        _ => println!("No item link at td.a href")
                    }
                }
            }
        }

        println!("{} total links collected\r\n", item_pages.len());

        // Find the link to the next page and update the url with it for the next iteration of the
        // crawl
        let next_page_selector = scraper::Selector::parse(r#"a[id="next1"]"#).unwrap();
        match document.select(&next_page_selector).next() {
            Some(next_node) => { 
                url = format!("{}{}{}", base_url.clone(), "?page=", next_node.inner_html());
                println!("Next page found at {}", url);
            },
            None => println!("No a tag with id='next1' found")
        }
    }
    Ok(())
}


async fn scrape_item_page<T>(url: T) -> Result<Item, Box<dyn std::error::Error>>
where T: IntoUrl + Clone
{

    let html_content = reqwest::get(url.clone()).await?.text().await?;
    let document = scraper::Html::parse_document(&html_content);

    let p_tag_selector = scraper::Selector::parse("p").unwrap();
    let th_tag_selector = scraper::Selector::parse("th").unwrap();
    let div_tag_selector = scraper::Selector::parse("div").unwrap();
    let object_tag_selector = scraper::Selector::parse("object").unwrap();

    let mut new_item = Item{..Default::default()};

    // Parse out item ID
    // Parse the URL string
    let url = reqwest::Url::parse(url.as_str()).unwrap();
    
    // Get the query pairs as a list of tuples
    let query_pairs = url.query_pairs();

    // Find the pair with key "id"
    for (key, value) in query_pairs {
        if key == "id" {
            new_item.id = value.into_owned();
        }
    }

    // Find image sprite url for download
    if let Some(object_tag) = document.select(&object_tag_selector).next() {
        let img_url = BASE_URL.to_owned() + object_tag.attr("data").unwrap_or("unknown.png");
        let img_response = reqwest::get(img_url).await?;
        let img_data = img_response.bytes().await.unwrap();

        // Save image
        let file_path = "imgs/".to_owned() + &new_item.id.clone() + ".png";
        let file = File::create(file_path);
        match file {
            Ok(mut f) => { let _ = f.write_all(&img_data); }
            _ => { println!("Error creating img file\r\n"); }
        }
    }


    // Parse out item name
    if let Some(th) = document.select(&th_tag_selector).next() {
        if let Some(div) = th.select(&div_tag_selector).next() {
            new_item.name = div.inner_html()
        }
    }

    for ptags in document.select(&p_tag_selector) {
        if ptags.inner_html().contains("Req LVL") {
            match ptags.inner_html().chars().filter(|c| c.is_digit(10)).collect::<String>().parse::<i32>() {
                Ok(n) => new_item.rlvl = Some(n),
                Err(e) => println!("Failed to parse Req LVL: {}", e),
            }
        }
        else if ptags.inner_html().contains("Req STR") {
            match ptags.inner_html().chars().filter(|c| c.is_digit(10)).collect::<String>().parse::<i32>() {
                Ok(n) => new_item.rstr = Some(n),
                Err(e) => println!("Failed to parse Req STR: {}", e),
            }
        }
        else if ptags.inner_html().contains("Req DEX") {
            match ptags.inner_html().chars().filter(|c| c.is_digit(10)).collect::<String>().parse::<i32>() {
                Ok(n) => new_item.rdex = Some(n),
                Err(e) => println!("Failed to parse Req DEX: {}", e),
            }
        }
        else if ptags.inner_html().contains("Req INT") {
            match ptags.inner_html().chars().filter(|c| c.is_digit(10)).collect::<String>().parse::<i32>() {
                Ok(n) => new_item.rint = Some(n),
                Err(e) => println!("Failed to parse Req INT: {}", e),
            }
        }
        else if ptags.inner_html().contains("Req LUK") {
            match ptags.inner_html().chars().filter(|c| c.is_digit(10)).collect::<String>().parse::<i32>() {
                Ok(n) => new_item.rluk = Some(n),
                Err(e) => println!("Failed to parse Req LUK: {}", e),
            }
        }
        else if ptags.inner_html().contains("Category:") {
            match ptags.inner_html().split_once(" "){
                Some((_, c)) => new_item.category = Some(c.strip_suffix("\n").unwrap().to_string()),
                _ => println!("Failed to get category string")
            }
        }
        else if ptags.inner_html().contains("Attack Speed") {
            match ptags.inner_html().chars().filter(|c| c.is_digit(10)).collect::<String>().parse::<i32>() {
                Ok(n) => new_item.attack_speed = Some(n),
                Err(e) => println!("Failed to parse attack speed: {}", e),
            }
        }
        else if ptags.inner_html().contains("Weapon Attack") {
            match ptags.inner_html().split_whitespace().nth(2) {
                Some(n) => match n.parse::<i32>() {
                    Ok(n) => new_item.weapon_attack= Some(n),
                    Err(e) => println!("Failed to parse weapon attack: {}", e),
                }
                _ => println!("Failed to get weapon attack"),
            }
        }
        else if ptags.inner_html().contains("Magic Attack") {
            match ptags.inner_html().split_whitespace().nth(2) {
                Some(n) => match n.parse::<i32>() {
                    Ok(n) => new_item.magic_attack= Some(n),
                    Err(e) => println!("Failed to parse magic attack: {}", e),

                }
                _ => println!("Failed to get magic attack"),
            }
        }
        else if ptags.inner_html().contains("STR") {
            match ptags.inner_html().split_whitespace().nth(1) {
                Some(n) => match n.parse::<i32>() {
                    Ok(n) => new_item.strn = Some(n),
                    Err(e) => println!("Failed to parse str: {}", e),
                }
                _ => println!("Failed to get str"),
            }
        }
        else if ptags.inner_html().contains("DEX") {
            match ptags.inner_html().split_whitespace().nth(1) {
                Some(n) => match n.parse::<i32>() {
                    Ok(n) => new_item.dex = Some(n),
                    Err(e) => println!("Failed to parse dex: {}", e),
                }
                _ => println!("Failed to get dex"),
            }
        }
        else if ptags.inner_html().contains("INT") {
            match ptags.inner_html().split_whitespace().nth(1) {
                Some(n) => match n.parse::<i32>() {
                    Ok(n) => new_item.inte = Some(n),
                    Err(e) => println!("Failed to parse int: {}", e),
                }
                _ => println!("Failed to get int"),
            }
        }
        else if ptags.inner_html().contains("LUK") {
            match ptags.inner_html().split_whitespace().nth(1) {
                Some(n) => match n.parse::<i32>() {
                    Ok(n) => new_item.luk = Some(n),
                    Err(e) => println!("Failed to parse luk: {}", e),
                }
                _ => println!("Failed to get luk"),
            }
        }

        else if ptags.inner_html().contains("HP:") {
            match ptags.inner_html().split_whitespace().nth(1) {
                Some(n) => match n.parse::<i32>() {
                    Ok(n) => new_item.hp = Some(n),
                    Err(e) => println!("Failed to parse hp: {}", e),
                }
                _ => println!("Failed to get hp"),
            }
        }
        else if ptags.inner_html().contains("MP:") {
            match ptags.inner_html().split_whitespace().nth(1) {
                Some(n) => match n.parse::<i32>() {
                    Ok(n) => new_item.mp = Some(n),
                    Err(e) => println!("Failed to parse mp: {}", e),
                }
                _ => println!("Failed to get mp"),
            }
        }
        else if ptags.inner_html().contains("Weapon Def:") {
            match ptags.inner_html().split_whitespace().nth(2) {
                Some(n) => match n.parse::<i32>() {
                    Ok(n) => new_item.weapon_def = Some(n),
                    Err(e) => println!("Failed to parse weapon def: {}", e),
                }
                _ => println!("Failed to get weapon def"),
            }
        }
        else if ptags.inner_html().contains("Magic Def:") {
            match ptags.inner_html().split_whitespace().nth(2) {
                Some(n) => match n.parse::<i32>() {
                    Ok(n) => new_item.magic_def = Some(n),
                    Err(e) => println!("Failed to parse magic def: {}", e),
                }
                _ => println!("Failed to get magic def"),
            }
        }
        else if ptags.inner_html().contains("Number of Upgrades Available") {
            match ptags.inner_html().chars().filter(|c| c.is_digit(10)).collect::<String>().parse::<i32>() {
                Ok(n) => new_item.slots_remaining = Some(n),
                Err(e) => println!("Failed to parse slots remaining: {}", e),
            }
        }
        else if ptags.inner_html().contains("Avoidability:") {
            match ptags.inner_html().split_whitespace().nth(1) {
                Some(n) => match n.parse::<i32>() {
                    Ok(n) => new_item.avoidability = Some(n),
                    Err(e) => println!("Failed to parse avoidability: {}", e),
                }
                _ => println!("Failed to get avoidability"),
            }
        }
        else if ptags.inner_html().contains("Accuracy:") {
            match ptags.inner_html().split_whitespace().nth(1) {
                Some(n) => match n.parse::<i32>() {
                    Ok(n) => new_item.accuracy = Some(n),
                    Err(e) => println!("Failed to parse accuracy: {}", e),
                }
                _ => println!("Failed to get accuracy"),
            }
        }
        else if ptags.inner_html().contains("Speed:") {
            match ptags.inner_html().split_whitespace().nth(1) {
                Some(n) => match n.parse::<i32>() {
                    Ok(n) => new_item.speed = Some(n),
                    Err(e) => println!("Failed to parse speed: {}", e),
                }
                _ => println!("Failed to get speed"),
            }
        }
        else if ptags.inner_html().contains("Jump:") {
            match ptags.inner_html().split_whitespace().nth(1) {
                Some(n) => match n.parse::<i32>() {
                    Ok(n) => new_item.jump = Some(n),
                    Err(e) => println!("Failed to parse jump: {}", e),
                }
                _ => println!("Failed to get jump"),
            }
        }
    }

    Ok(new_item)
}

// Inserts an item struct into the db
pub async fn insert_item(pool: &PgPool, item: &Item) -> Result<(), sqlx::Error> {

    sqlx::query!(
        "INSERT INTO items (name, id, rlvl, rstr, rdex, rint, rluk, category, attack_speed,
                            weapon_attack, magic_attack, strn, dex, inte, luk, hp, mp, weapon_def, magic_def,
                            slots_remaining, avoidability, accuracy, speed, jump)
        VALUES ($1, $2, $3, $4, $5, $6, $7,
                $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20, $21, $22, $23, $24)",

        item.name, item.id, item.rlvl, item.rstr, item.rdex, item.rint, item.rluk, item.category,
        item.attack_speed, item.weapon_attack, item.magic_attack, item.strn, item.dex, item.inte,
        item.luk, item.hp, item.mp, item.weapon_def, item.magic_def, item.slots_remaining,
        item.avoidability, item.accuracy, item.speed, item.jump,
    )
    .execute(pool)
    .await?;

    Ok(())
}
