-- Add migration script here
CREATE TABLE items (
    name VARCHAR(100) NOT NULL,
    id VARCHAR(100) NOT NULL,
    rlvl INTEGER,
    rstr INTEGER,
    rdex INTEGER,
    rint INTEGER,
    rluk INTEGER,
    category VARCHAR(100),
    attack_speed INTEGER,
    weapon_attack INTEGER,
    magic_attack INTEGER,
    strn INTEGER,
    dex INTEGER,
    inte INTEGER,
    luk INTEGER,
    hp INTEGER,
    mp INTEGER,
    weapon_def INTEGER,
    magic_def INTEGER,
    slots_remaining INTEGER,
    avoidability INTEGER,
    accuracy INTEGER,
    speed INTEGER,
    jump INTEGER
);

